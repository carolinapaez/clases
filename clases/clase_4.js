// strings 1

var miString1= "hola como estas"
alert (miString1);

//string 2

const miString2="Muy bien, gracias "
console.log (miString2);

//string 3

let miString3="y tu?"
console.log (miString3);

// string 3.a

const nombre="Pedro"
let miString3 ='y tu ${nombre}?'
console.log (miString3);

//string 3.b

let miString3= 'y tu'+ nombre + '?'
console.log (miString3);

//acceso a caracteres (chatAt)

const nombre='Pedro'
let miString3 = 'y tu ${nombre}?'
console.log (miString3.charAt(5));

//tratar string como arrays

const miString4= "Erase una vez"
for (i=0;i<miString4.length; i++){
    console.log (miString4 /faltan corchetes )
}

//comparar string

var string1="hola"
var string2= " hola"
console.log (string1==string2)

